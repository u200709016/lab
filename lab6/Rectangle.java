public class Rectangle {

    private Point topLeft;
    private int width;
    private int height;

    public Rectangle(Point topLeft, int width, int height) {
        this.topLeft = topLeft;
        this.width = width;
        this.height = height;

    }

    public int area() {
        return width * height;
    }

    public int perimeter() {
        return 2 * (width + height);
    }

    public Point[] corners(){
        Point[] corners = new Point[4];
        corners[0] = topLeft;
        corners[1] = new Point(topLeft.getxCord() + width, topLeft.getyCord());
        corners[2] = new Point(topLeft.getxCord() + width, topLeft.getyCord() - height);
        corners[3] = new Point(topLeft.getxCord(), topLeft.getyCord() - height);

        return corners;
    }
}
